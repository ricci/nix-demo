"""
This is a simple profile that has the nix pacakge manager installed on top of the default Emulab/CloudLab Ubuntu 20.04 image.

Instructions:
If your shell can't find the `nix` commands (eg. `nix-shell`), source
```
/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
```
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg

# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

portal.context.defineParameter( "flake", "Home-manager flake (full URI)", portal.ParameterType.STRING, "" )
portal.context.defineParameter( "user", "User to set up (cloudlab username)", portal.ParameterType.STRING, "" )
params = portal.context.bindParameters()

# Add a raw PC to the request.
node = request.RawPC("node")
node.disk_image = "urn:publicid:IDN+utah.cloudlab.us+image+testbed-PG0:nix-22.05-ubuntu20"

# Install and execute a script that is contained in the repository.
node.addService(pg.Execute(shell="sh", command="/local/repository/nix-home-setup.sh {} {}".format(params.flake,params.user)))

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
