#!/bin/sh

sudo -u $2 -Hi nix-env -i home-manager
sudo -u $2 -Hi home-manager --flake $1 -b backup switch
